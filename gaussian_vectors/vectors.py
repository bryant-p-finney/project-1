import numpy as np

from matplotlib import pyplot as plt


class CorrelatedVectorGenerator(object):

    def __init__(self, K, mu):
        """
        Instantiate a new CorrelatedVectorGenerator object with the given properties.

        K is an n x n covariance matrix describing the gaussian distribution to use
        mu is the mean accross each dimenstion
        """
        shape = K.shape
        if shape[0] is not shape[1] or len(mu) is not shape[1]:
            raise ValueError("Covariance matrix 'K' must bw an nxn matrix, and mean vector 'mu' must be an nx1" +
                             "column vector")

        self.covariance_matrix = K
        self.mean_vector = np.reshape(mu, (K.shape[0], 1))

    def generate_new(self, nvectors):
        """
        Generate 'nvectors' gaussian vectors.

        Returns an n x N array, where 'n' is the number of dimensions for each RV, and N is the
        provided number of vectors
        """
        X = np.empty((len(self.mean_vector), nvectors))
        for icol in range(nvectors):
            X[:, icol] = np.random.multivariate_normal(
                self.mean_vector.squeeze(), self.covariance_matrix)
        return X

    def whiten(self, X):
        """
        Apply a whitening transform from a singular value decomposition of the covariance matrix
        """
        whitening_matrix, _, _ = np.linalg.svd(self.covariance_matrix)
        return whitening_matrix, np.matmul(whitening_matrix, X - self.mean_vector)


def _plot_vectors(ax, vectors, title):
    """ helper function; plots the n x N array of RVs """
    ax.plot(vectors[0, :], vectors[1, :], marker='x', linestyle='', label="RVs")
    ax.plot(np.mean(vectors[0, :]), np.mean(vectors[1, :]), marker='o', label="mu")
    ax.set_title(title)
    ax.grid(True)
    ax.legend()


def main():
    gen = CorrelatedVectorGenerator(np.array([[3, -1], [-1, 2]]), np.array([2, 1]))
    normal_vectors = gen.generate_new(1024)

    ax = plt.subplot(1, 2, 1)
    _plot_vectors(ax, normal_vectors, "Gaussian RVs")

    whitening_matrix, whitened_vectors = gen.whiten(normal_vectors)

    ax = plt.subplot(1, 2, 2, sharey=ax, sharex=ax)
    _plot_vectors(ax, whitened_vectors, "Whitened RVs")

    print(f"Initial covariance: \n{np.cov(normal_vectors)}", end="\n\n")

    print(f"Whitening matrix: \n{whitening_matrix}", end="\n\n")

    print(f"Whitened covariance: \n{np.cov(whitened_vectors)}")

    plt.show()


if __name__ == "__main__":
    main()
